# SparticleのエフェクトにAway3Dに組み込む方法 #

![bomb.PNG](https://bitbucket.org/repo/5qqbjaj/images/4186501319-bomb.PNG)
![fire.PNG](https://bitbucket.org/repo/5qqbjaj/images/1412240467-fire.PNG)

## 実際のエフェクト
- EffectHub.com: Your best source for gaming : http://www.effecthub.com/user/1000097

## 組み込み手順

1. Sparticleのパーサーを追加  
https://bitbucket.org/orangesuzuki/sparticle-test/commits/58661fc59081327543dff4079d762554aaf20fd4?at=master

1. Away3D_dev版へアップデートする  
https://bitbucket.org/orangesuzuki/sparticle-test/commits/94aa7c1de130c21c913d6eabe86100e9e94c3b2e?at=master

1. エフェクトクラスを追加 
https://bitbucket.org/orangesuzuki/sparticle-test/src/8cbe4edc9391d119fe74b0890b047e32201e97d9/src/effects/FireEffect.as?at=master&fileviewer=file-view-default

1. エフェクトを3Dシーンに追加  
https://bitbucket.org/orangesuzuki/sparticle-test/commits/01d54386080d797aadc4026e521a316965f8015f?at=master