package away3d.animators.nodes
{
	import away3d.*;
	import away3d.animators.*;
	import away3d.animators.data.*;
	import away3d.animators.states.*;
	import away3d.materials.compilation.*;
	import away3d.materials.passes.*;
	
	use namespace arcane;
	
	/**
	 * A particle animation node used to create a follow behaviour on a particle system.
	 */
	public class ParticleFollowNode extends ParticleNodeBase
	{
		/** @private */
		arcane static const FOLLOW_POSITION_INDEX:uint = 0;
		
		/** @private */
		arcane static const FOLLOW_ROTATION_INDEX:uint = 1;
		
		/** @private */
		arcane var _usesPosition:Boolean;
		
		/** @private */
		arcane var _usesRotation:Boolean;
						
		/**
		 * Creates a new <code>ParticleFollowNode</code>
		 *
		 * @param    [optional] usesPosition     Defines wehether the individual particle reacts to the position of the target.
		 * @param    [optional] usesRotation     Defines wehether the individual particle reacts to the rotation of the target.
		 */
		public function ParticleFollowNode(usesPosition:Boolean = true, usesRotation:Boolean = true)
		{
			_stateClass = ParticleFollowState;
			
			_usesPosition = usesPosition;
			_usesRotation = usesRotation;
			var len:int;
			if (_usesPosition)
				len += 3;
			if (_usesRotation)
				len += 4;
			super("ParticleFollow", ParticlePropertiesMode.LOCAL_DYNAMIC, len, ParticleAnimationSet.POST_PRIORITY);
		}
		
		/**
		 * @inheritDoc
		 */
		override public function getAGALVertexCode(pass:MaterialPassBase, animationRegisterCache:AnimationRegisterCache):String
		{
			var code:String = "";
			if (_usesRotation) {
				var rotationAttribute:ShaderRegisterElement = animationRegisterCache.getFreeVertexAttribute();
				animationRegisterCache.setRegisterIndex(this, FOLLOW_ROTATION_INDEX, rotationAttribute.index);
				
				var temp1:ShaderRegisterElement = animationRegisterCache.getFreeVertexVectorTemp();
				animationRegisterCache.addVertexTempUsages(temp1, 1);
				var temp2:ShaderRegisterElement = animationRegisterCache.getFreeVertexVectorTemp();
				animationRegisterCache.addVertexTempUsages(temp2, 1);
				var temp3:ShaderRegisterElement = animationRegisterCache.getFreeVertexVectorTemp();
				
				animationRegisterCache.removeVertexTempUsage(temp1);
				animationRegisterCache.removeVertexTempUsage(temp2);
				
				var len:int = animationRegisterCache.rotationRegisters.length;
				var i:int;
				
				//rev_R
				code += "neg " + temp1 + "," + rotationAttribute + "\n";
				code += "neg " + temp1 + ".w," + rotationAttribute + ".w\n";
				
				code += "crs " + temp2 + ".xyz," + rotationAttribute + ".xyz," + animationRegisterCache.scaleAndRotateTarget + "\n";
				code += "mul " + temp3 + ".xyz," + rotationAttribute +".w," + animationRegisterCache.scaleAndRotateTarget + "\n";
				code += "add " + temp2 + ".xyz," + temp2 +".xyz," + temp3 + ".xyz\n";
				code += "dp3 " + temp3 + ".w," + rotationAttribute + ".xyz," +animationRegisterCache.scaleAndRotateTarget + "\n";
				code += "neg " + temp2 + ".w," + temp3 + ".w\n";
				
				code += "crs " + temp3 + ".xyz," + temp2 + ".xyz," +temp1 + "\n";
				
				code += "mul " + temp1 + ".xyzw,"+temp2+","+
				
				////
				code += "crs " + nrmVel + ".xyz," + R + ".xyz," +animationRegisterCache.scaleAndRotateTarget + ".xyz\n";

				code += "mul " + xAxis + ".xyz," + cos +"," + animationRegisterCache.scaleAndRotateTarget + "\n";
				code += "add " + nrmVel + ".xyz," + nrmVel +".xyz," + xAxis + ".xyz\n";
				code += "dp3 " + xAxis + ".w," + R + "," +animationRegisterCache.scaleAndRotateTarget + "\n";
				code += "neg " + nrmVel + ".w," + xAxis + ".w\n";
				
				
				code += "crs " + R + "," + nrmVel + ".xyz," +R_rev + "\n";

				//use cos as R_rev.w
				code += "mul " + xAxis + ".xyzw," + nrmVel + ".xyzw," +cos + "\n";
				code += "add " + R + "," + R + "," + xAxis + ".xyz\n";
				code += "mul " + xAxis + ".xyz," + nrmVel + ".w," +R_rev + "\n";
				
				code += "add " + animationRegisterCache.scaleAndRotateTarget + "," + R + "," + xAxis + ".xyz\n";
				////
				
				code += "crs " + animationRegisterCache.scaleAndRotateTarget + "," + temp2 + "," + temp1 + "\n";
			}
			
			if (_usesPosition) {
				var positionAttribute:ShaderRegisterElement = animationRegisterCache.getFreeVertexAttribute();
				animationRegisterCache.setRegisterIndex(this, FOLLOW_POSITION_INDEX, positionAttribute.index);
				code += "add " + animationRegisterCache.scaleAndRotateTarget + "," + positionAttribute + "," + animationRegisterCache.scaleAndRotateTarget + "\n";
			}
			
			return code;
		}
		
		/**
		 * @inheritDoc
		 */
		public function getAnimationState(animator:IAnimator):ParticleFollowState
		{
			return animator.getAnimationState(this) as ParticleFollowState;
		}
	}

}
