package effects
{
	import flash.net.URLRequest;
	
	import away3d.containers.ObjectContainer3D;
	import away3d.entities.ParticleGroup;
	import away3d.events.AssetEvent;
	import away3d.events.LoaderEvent;
	import away3d.events.ParticleGroupEvent;
	import away3d.library.assets.AssetType;
	import away3d.loaders.AssetLoader;

	public class FireEffect extends ObjectContainer3D
	{

		//----------------------------------------------------------
		//
		//   Property 
		//
		//----------------------------------------------------------

		public var particleGroup:ParticleGroup;

		//----------------------------------------------------------
		//
		//   Constructor 
		//
		//----------------------------------------------------------

		public function FireEffect()
		{
			super();

//			var effectUrl:String = "effect/fire/effect_fire.awp";
			var effectUrl:String = "effect/bomb/bomb.awp";
			
			
			var loader:AssetLoader = new AssetLoader();
			loader.addEventListener(AssetEvent.ASSET_COMPLETE, onAssetComplete);
			loader.addEventListener(LoaderEvent.RESOURCE_COMPLETE, onComplete);
			loader.load(new URLRequest(effectUrl));
		}

		private function onAssetComplete(e:AssetEvent):void
		{
			trace("onAssetComplete");
			if (e.asset.assetType == AssetType.CONTAINER && e.asset is ParticleGroup)
			{
				particleGroup = e.asset as ParticleGroup;
				addChild(particleGroup);
				
				particleGroup.animator.addEventListener(ParticleGroupEvent.OCCUR, animationStopHandler);
				particleGroup.animator.start();
			}
		}
		
		private function onComplete(e:LoaderEvent):void
		{
			trace("Loader Complete");
		}
		
		protected function animationStopHandler(event:ParticleGroupEvent):void
		{			
			trace("animationStopHandler", event);
		}
	}
}
